use dynomite::{Item, FromAttributes};
use futures::compat::Compat01As03;
use rusoto_dynamodb::{AttributeValue, DynamoDb, DynamoDbClient, GetItemInput};
use uuid::Uuid;

use std::collections::HashMap;

#[derive(Item)]
pub struct Card {
    #[dynomite(partition_key)]
    pub id: Uuid,
    pub name: String,
    #[dynomite(rename = "type")]
    pub type_: String, // type is a reserved keyword
    pub class: String,
    pub description: String,
    pub health: u32,
    pub insight: u32,
}

pub async fn get_card(client: &DynamoDbClient, id: Uuid) -> Option<Card> {
    println!("Key: {}", id.to_string());
    let fut = client.get_item(GetItemInput{
        table_name: super::CARD_TABLE_NAME.into(),
        key: {
            let mut map = HashMap::with_capacity(1);
            map.insert("id".into(), AttributeValue{ s: Some(id.to_string()), ..AttributeValue::default()});
            map
        },
        .. GetItemInput::default()
    });
    let output = Compat01As03::new(fut).await.ok()?;
    output.item.map(Card::from_attrs).and_then(Result::ok)
}
