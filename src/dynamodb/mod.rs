/// This module is for dynamodb-specific code.

mod card; pub use card::*;

pub const CARD_TABLE_NAME: &str = "rathe-cards";