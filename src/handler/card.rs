use uuid::Uuid;

use super::super::{Context, dynamodb::get_card};

pub async fn card_handler(ctx: Context) -> String {
    let card = get_card(ctx.state(), ctx.param::<Uuid>("id").expect("Could not understand card id!"))
        .await
        .expect("No card with that id found.");
    format!(
        "Name: {}\nType: {}\nClass {}",
        card.name, card.type_, card.class
    )
}
