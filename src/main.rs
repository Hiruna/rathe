use rusoto_core::Region;
use rusoto_dynamodb::DynamoDbClient;
use tide::{App, Context as TideContext};

mod dynamodb;
mod handler;

use handler::*;

type Context = TideContext<(DynamoDbClient)>;

fn main() {
    let db = DynamoDbClient::new(Region::ApSoutheast2);
    let mut app = App::with_state(db);
    app.at("/card/:id").get(card_handler);
}
